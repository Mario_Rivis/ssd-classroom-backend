package ro.upt.ac.ssdclassroom.web.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.upt.ac.ssdclassroom.model.Student;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4300")
public class StudentController {

    @GetMapping("/hello")
    public String sayHello() {
        return "Hello World!";
    }

    @GetMapping("/students")
    public List<Student> getAllStudents() {
        return Arrays.asList(
                new Student("Ana", 20, true),
                new Student("Vlad", 20, true),
                new Student("Paul", 20, false),
                new Student("John", 20, false),
                new Student("dsafdafds", 20, true)
        );
    }
}
