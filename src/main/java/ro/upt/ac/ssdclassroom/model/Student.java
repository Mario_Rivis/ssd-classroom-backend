package ro.upt.ac.ssdclassroom.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Student {
    private String name;
    private int age;

    @JsonProperty("isPresent")
    private boolean isPresent;
}
