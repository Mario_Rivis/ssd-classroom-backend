package ro.upt.ac.ssdclassroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsdClassroomApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsdClassroomApplication.class, args);
    }
}
